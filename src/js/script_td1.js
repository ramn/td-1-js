//let num = 1;

function adapterGalerie(nom) {
    adapterTitre(nom);
    for(let i = 1; i <= 6; i++) {
        const image = document.getElementById('fleur' + i);
        image.src = 'img/fleurs/' + nom + '/' + nom + i + '.jpg';
        image.title = nom;
        image.alt = nom + i;
        // ou avec la syntaxe `` qui permet le remplacement de variables
        // image.src = `img/fleurs/${nom}/${nom}${i}.jpg`
    }
    /*    const imgBan1 = document.getElementById(num);
        num = suivant(num);
        const imgBan2 = document.getElementById(num);
        cacher(imgBan1);
        afficher(imgBan2);*/
}
function adapterTitre(nom){
    //document.getElementById("tite").textContent = tabTitres[nom];
    document.title =tabTitres[nom];
    document.getElementById("titre").textContent = tabTitres[nom];
}
function stopperDefilement (){

    clearInterval(chb);
}
function lancerDefilement(){
    chb = setInterval(changeBanniereV1, 6000);
}

/**
 * @param {HTMLElement} im
 */
function cacher(im) {
    im.classList.add('cachee');
    im.classList.remove('visible');
}
/**
 * @param {HTMLElement} im
 */
function afficher(im) {
    im.classList.remove('cachee');
    im.classList.add('visible');
}

function suivant(n) {
    return (n)%6 +1;
}

function changeBanniereV1(){
    const imgBan1 = document.getElementsByClassName("visible");
    const imgBan2 = document.getElementById(suivant(imgBan1[0].id));
    imgBan1[0].style.transition = "opacity 3s";
    cacher(imgBan1[0]);
    imgBan2.style.transition = "opacity 3s";
    afficher(imgBan2);
}

function construitInfobulle() {
    const info = document.createElement('div');
    info.innerHTML = "<p>c'est moi la bulle !</p>";
    info.id = "bulle";
    info.style.position = "fixed";
    info.style.top = "100px";
    info.style.right = "150px";
    info.style.backgroundColor = "darkblue";
    info.style.color = "white";
    document.body.appendChild(info);
}
function detruitInfobulle() {
    const info = document.getElementById('bulle');
    document.body.removeChild(info);
}
function changerParametres() {
    let int = 1;
    do {
        int = Math.floor(Math.random() * (5 - 1) + 1);
    }while('url(img/background/bg-' +int+ '.jpg)' ===  document.body.style.backgroundImage);
    document.body.style.backgroundImage = 'url(img/background/bg-' +int+ '.jpg)';
}

let chb = setInterval(changeBanniereV1, 6000);

const tabTitres = {
    'rose' : 'Galerie de roses',
    'hortensia': 'Galerie d’hortensias',
    'fruitier': 'Galerie de fruitiers',
    'autre': 'Galerie de fleurs diverses'
};